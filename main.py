plansza = [[" ", "x", " ", "x", " ", "x", " ", "x"],
           ["x", " ", "x", " ", "x", " ", "x", " "],
           [" ", "x", " ", "x", " ", "x", " ", "x"],
           [" ", " ", " ", " ", " ", " ", " ", " "],
           [" ", " ", " ", " ", " ", " ", " ", " "],
           ["o", " ", "o", " ", "o", " ", "o", " "],
           [" ", "o", " ", "o", " ", "o", " ", "o"],
           ["o", " ", "o", " ", "o", " ", "o", " "]]

liczby = ["1", "2", "3", "4", "5", "6", "7", "8"]
litery = ["A", "B", "C", "D", "E", "F", "G", "H"]
gracz = "o"
punkty_o = 0
punkty_x = 0
zostalo_o = 12
zostalo_x = 12

print("   ", " ".join(liczby))
for i in range(8):
    print(litery[i], "|", "|".join(plansza[i]), "|")
while zostalo_o > 0 and zostalo_x > 0:
    print(f"Ruch gracza: {gracz}")
    ruch = input("Podaj Literę i Numer pola, z którego ruszasz: ")
    if len(ruch) != 2:
        print("Podaj tylko Literę i Numer")
        continue
    if ruch[0].upper() in litery:
        rzad = litery.index(ruch[0].upper())
    else:
        print("Błędny rzad")
        continue
    if ruch[1] in liczby:
        kolumna = int(ruch[1])-1
    else:
        print("Błędna kolumna")
        continue
    if plansza[rzad][kolumna] != gracz:
        print("To miejsce nie jest zajęte przez Ciebie")
        continue

    cel = input("Podaj Lierę i Numer pola, na które chcesz ustawić pionek: ")
    if len(cel) != 2:
        print("Podaj tylko Literę i Numer")
        continue
    if cel[0].upper() in litery:
        rzad2 = litery.index(cel[0].upper())
    else:
        print("Błędny rzad")
        continue
    if cel[1] in liczby:
        kolumna2 = int(cel[1])-1
    else:
        print("Błędna kolumna")
        continue
    if plansza[rzad2][kolumna2] != " ":
        print("To miejsce jest zajęte")
        continue
    if gracz == "o":
        if (rzad2 + 1) == rzad:
            if (kolumna2 + 1) == kolumna or (kolumna2 - 1) == kolumna:
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
            else:
                print("Nieprawidłowy ruch")
                continue
        elif (rzad2 + 2) == rzad:
            if (kolumna2 + 2) == kolumna and (plansza[rzad-1][kolumna-1]) == "x":
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
                plansza[rzad-1][kolumna-1] = " "
                punkty_o += 1
                zostalo_x -= 1
                print("Zbiełeś pionek przeciwnika.")
            elif (kolumna2 - 2) == kolumna and (plansza[rzad - 1][kolumna + 1]) == "x":
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
                plansza[rzad - 1][kolumna + 1] = " "
                punkty_o += 1
                zostalo_x -= 1
                print("Zbiełeś pionek przeciwnika.")
            else:
                print("Nieprawidlowy ruch")
                continue
        else:
            print("Nieprawidłowy ruch")
            continue
    elif gracz == "x":
        if (rzad2 - 1) == rzad:
            if (kolumna2 + 1) == kolumna or (kolumna2 - 1) == kolumna:
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
            else:
                print("Nieprawidłowy ruch")
                continue
        elif (rzad2 - 2) == rzad:
            if (kolumna2 + 2) == kolumna and (plansza[rzad+1][kolumna-1]) == "o":
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
                plansza[rzad+1][kolumna-1] = " "
                punkty_x += 1
                zostalo_o -= 1
                print("Zbiełeś pionek przeciwnika.")
            elif (kolumna2 - 2) == kolumna and (plansza[rzad + 1][kolumna + 1]) == "o":
                plansza[rzad][kolumna] = " "
                plansza[rzad2][kolumna2] = gracz
                plansza[rzad + 1][kolumna + 1] = " "
                punkty_x += 1
                zostalo_o -= 1
                print("Zbiełeś pionek przeciwnika.")
            else:
                print("Nieprawidlowy ruch")
                continue
        else:
            print("Nieprawidłowy ruch")
            continue
    else:
        print("Nieprawidłowy ruch")
        continue
    print("   ", " ".join(liczby))
    for i in range(8):
        print(litery[i], "|", "|".join(plansza[i]), "|")
    if gracz == "o":
        print("Gracz o:")
        print(f"Masz {punkty_o} punktow")
        gracz = "x"
    else:
        print("Gracz x:")
        print(f"Masz {punkty_x} punktow")
        gracz = "o"

print("Koniec gry")
if zostalo_o == 0:
    print("Wygrał gracz x")
else:
    print("Wygral gracz o")







